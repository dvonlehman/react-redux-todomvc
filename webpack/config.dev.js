'use strict';
/* eslint no-var:0 */
var merge             = require('lodash.merge');
var webpack           = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var baseConfig        = require('./config.base');

module.exports = merge(baseConfig, {
  entry: baseConfig.entry.concat([
    'webpack-hot-middleware/client',
  ]),

  output: {
    filename: 'static/bundle.js',
  },

  module: {
    loaders: baseConfig.module.loaders.concat([
      {
        test: /\.css$/,
        loaders: [
          'style',
          'css?modules&sourceMap&localIdentName=[name]---[local]---[hash:base64:5]',
          'postcss',
        ],
      },
    ]),
  },

  devtool: 'cheap-module-eval-source-map',

  plugins: baseConfig.plugins.concat([
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new HtmlWebpackPlugin({ template: 'template.html', hash: true, inject: false }),
  ]),
});
