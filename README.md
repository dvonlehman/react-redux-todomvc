# react-redux-todomvc

This repository was forked from [exeto/react-redux-starter](https://github.com/exeto/react-redux-starter) for purposes of demonstrating the [Aerobatic npm build pipeline](https://www.aerobatic.com/docs/automated-builds#npm).

See a live demo at [https://react-redux-todomvc.aerobatic.io](https://react-redux-todomvc.aerobatic.io).

